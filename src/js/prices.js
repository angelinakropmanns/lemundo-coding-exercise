const userInput = document.querySelector('[name=user-input]')
const resultInput = document.querySelector('[name=result]')
const resultButton = document.querySelector('.result-button')

const updateResult = () => {
  const userInputValue = userInput.value
  if (userInputValue <= 2) {
    const result = (userInputValue * 19.95 * 0.9).toFixed(2)
    resultInput.value = result
  } else if (userInputValue == 3) {
    const result = (userInputValue * 19.95 * 0.8).toFixed(2)
    resultInput.value = result
  } else if (userInputValue >= 5) {
    const result = (userInputValue * 19.95 * 0.7).toFixed(2)
    resultInput.value = result
  }
}

resultButton.addEventListener('click', updateResult)
