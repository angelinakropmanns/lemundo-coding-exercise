const generalDesk = document.getElementById('desktop-general')
const engineDesk = document.getElementById('desktop-engine')
const specialsDesk = document.getElementById('desktop-specials')
const generalMobile = document.getElementById('mobile-general')
const engineMobile = document.getElementById('mobile-engine')
const specialsMobile = document.getElementById('mobile-specials')

const generalTab = document.querySelector('.general')
const engineTab = document.querySelector('.engine')
const specialsTab = document.querySelector('.specials')

generalTab.addEventListener('click', (event) => {
  event.preventDefault()
  generalDesk.classList.remove('hidden')
  engineDesk.classList.add('hidden')
  specialsDesk.classList.add('hidden')
  localStorage.setItem('active', 'general')
})

engineTab.addEventListener('click', (event) => {
  event.preventDefault()
  generalDesk.classList.add('hidden')
  engineDesk.classList.remove('hidden')
  specialsDesk.classList.add('hidden')
  localStorage.setItem('active', 'engine')
})

specialsTab.addEventListener('click', (event) => {
  event.preventDefault()
  generalDesk.classList.add('hidden')
  engineDesk.classList.add('hidden')
  specialsDesk.classList.remove('hidden')
  localStorage.setItem('active', 'specials')
})

generalTab.addEventListener('click', (event) => {
  event.preventDefault()
  generalMobile.classList.remove('hidden')
  engineMobile.classList.add('hidden')
  specialsMobile.classList.add('hidden')
  localStorage.setItem('active', 'general')
})

engineTab.addEventListener('click', (event) => {
  event.preventDefault()
  generalMobile.classList.add('hidden')
  engineMobile.classList.remove('hidden')
  specialsMobile.classList.add('hidden')
  localStorage.setItem('active', 'engine')
})

specialsTab.addEventListener('click', (event) => {
  event.preventDefault()
  generalMobile.classList.add('hidden')
  engineMobile.classList.add('hidden')
  specialsMobile.classList.remove('hidden')
  localStorage.setItem('active', 'specials')
})

if (localStorage.getItem('active') == 'engine') {
  generalDesk.classList.add('hidden')
  engineDesk.classList.remove('hidden')
  specialsDesk.classList.add('hidden')
  generalMobile.classList.add('hidden')
  engineMobile.classList.remove('hidden')
  specialsMobile.classList.add('hidden')
} else if (localStorage.getItem('active') == 'specials') {
  generalDesk.classList.add('hidden')
  engineDesk.classList.add('hidden')
  specialsDesk.classList.remove('hidden')
  generalMobile.classList.add('hidden')
  engineMobile.classList.add('hidden')
  specialsMobile.classList.remove('hidden')
} else {
  generalDesk.classList.remove('hidden')
  engineDesk.classList.add('hidden')
  specialsDesk.classList.add('hidden')
  generalMobile.classList.remove('hidden')
  engineMobile.classList.add('hidden')
  specialsMobile.classList.add('hidden')
}
