const generalTab = document.querySelector('.general')
const engineTab = document.querySelector('.engine')
const specialsTab = document.querySelector('.specials')

generalTab.addEventListener('click', (event) => {
  event.preventDefault()
  generalTab.classList.add('active-tab')
  engineTab.classList.remove('active-tab')
  specialsTab.classList.remove('active-tab')
  localStorage.setItem('active', 'general')
})

engineTab.addEventListener('click', (event) => {
  event.preventDefault()
  generalTab.classList.remove('active-tab')
  engineTab.classList.add('active-tab')
  specialsTab.classList.remove('active-tab')
  localStorage.setItem('active', 'engine')
})

specialsTab.addEventListener('click', (event) => {
  event.preventDefault()
  generalTab.classList.remove('active-tab')
  engineTab.classList.remove('active-tab')
  specialsTab.classList.add('active-tab')
  localStorage.setItem('active', 'specials')
})

if (localStorage.getItem('active') == 'engine') {
  generalTab.classList.remove('active-tab')
  engineTab.classList.add('active-tab')
  specialsTab.classList.remove('active-tab')
} else if (localStorage.getItem('active') == 'specials') {
  generalTab.classList.remove('active-tab')
  engineTab.classList.remove('active-tab')
  specialsTab.classList.add('active-tab')
} else {
  generalTab.classList.add('active-tab')
  engineTab.classList.remove('active-tab')
  specialsTab.classList.remove('active-tab')
}
